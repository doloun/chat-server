package com.avg.chat;

/**
 * @author Martin Dolozilek <martin.dolozilek@avg.com>
 */
public class Greeting {

    private String content;

    public Greeting(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

}
